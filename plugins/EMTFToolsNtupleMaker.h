#ifndef EMTFTools_NtupleMaker_EMTFToolsNtupleMaker
#define EMTFTools_NtupleMaker_EMTFToolsNtupleMaker

#include <memory>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "EMTFTools/NtupleMaker/interface/DynamicTree.h"
#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

class EMTFToolsNtupleMaker: public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {
    public:
        explicit EMTFToolsNtupleMaker(const edm::ParameterSet&);
        ~EMTFToolsNtupleMaker() override;

        static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

    private:
        void beginJob() override;
        void analyze(const edm::Event&, const edm::EventSetup&) override;
        void endJob() override;

        void beginRun(const edm::Run&, const edm::EventSetup&) override;
        void endRun(const edm::Run&, const edm::EventSetup&) override;
        
        emtf::tools::NtupleMakerContext context_; 
        emtf::tools::DynamicTree tree_; 
        std::vector<std::unique_ptr<emtf::tools::DataCollector>> data_collectors_;
};

#endif

