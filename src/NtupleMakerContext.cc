#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFConfiguration.h"

using namespace emtf::tools;

NtupleMakerContext::NtupleMakerContext(
        const edm::ParameterSet& pset,
        edm::ConsumesCollector consumes_collector
):
    gen_part_descendent_finder_(pset, consumes_collector),
    subsystem_mc_truth_(pset, consumes_collector)
{
    // Do Nothing
}

NtupleMakerContext::~NtupleMakerContext() {
    // Do Nothing
}

void NtupleMakerContext::update(
        const edm::Event& evt,
        const edm::EventSetup& evt_setup
) {
    gen_part_descendent_finder_.update(evt, evt_setup);
    subsystem_mc_truth_.update(evt, evt_setup);
}

