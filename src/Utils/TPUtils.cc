#include <cmath>

#include "EMTFTools/NtupleMaker/interface/Utils/TPUtils.h"

namespace emtf::tools {

    float deg_to_rad(float deg) {
        constexpr float factor = M_PI / 180.;
        return deg * factor;
    }

    float rad_to_deg(float rad) {
        constexpr float factor = 180. / M_PI;

        return rad * factor;
    }


}
