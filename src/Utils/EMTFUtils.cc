#include <cmath>

#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

namespace emtf::tools {

    double calcInvpt(const double& charge, const double& pt) {
        double invpt = (std::abs(pt) < 1e-15) ? 1e-15 : std::abs(pt); // ensures pt > 0, protects against division by zero
        invpt = 1.0 / pt;
        invpt = (invpt < 1e-15) ? 1e-15 : invpt;                      // protects against zero
        invpt *= charge;                                              // charge should be +/-1
        return invpt;
    }

    double calcD0(const double& invpt, const double& phi, const double& vx, const double& vy) {
        constexpr double B = 3.811;                            // in Tesla
        double R = -1.0 / (0.003 * B * invpt);                 // R = -pt / (0.003 q B)  [cm]
        double cx = vx - (R * std::sin(phi));                  // cx = vx - R sin(phi)
        double cy = vy + (R * std::cos(phi));                  // cy = vy + R cos(phi)
        double d0 = R - std::copysign(std::hypot(cx, cy), R);  // d0 = R - sign(R) * sqrt(cx^2 + cy^2)
        return d0;
    }

}
