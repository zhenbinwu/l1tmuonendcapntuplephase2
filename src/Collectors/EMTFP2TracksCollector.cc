#include <numeric>

#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2TracksCollector.h"

using namespace emtf::tools;

EMTFP2TracksCollector::EMTFP2TracksCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    token_  = consumes_collector.consumes<emtf::phase2::EMTFTrackCollection>(
            pset.getParameter<edm::InputTag>("EMTFP2TrackTag")  
    );
}

EMTFP2TracksCollector::~EMTFP2TracksCollector() {
    // Do Nothing
}

void EMTFP2TracksCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_trk_endcap>();       
    tree.decfield<tag_trk_sector>();       
    tree.decfield<tag_trk_bx>();           
    tree.decfield<tag_trk_invpt>();           
    tree.decfield<tag_trk_phi>();          
    tree.decfield<tag_trk_theta>();        
    tree.decfield<tag_trk_eta>();          
    tree.decfield<tag_trk_d0>();           
    tree.decfield<tag_trk_q>();            
    tree.decfield<tag_trk_unconstrained>();
    tree.decfield<tag_trk_hitmode>();      
    tree.decfield<tag_trk_nhits>();        
    tree.decfield<tag_trk_valid>();        
    
    tree.decfield<tag_trk_hw_qual>();      
    tree.decfield<tag_trk_hw_pt>();        
    tree.decfield<tag_trk_hw_eta>();       
    tree.decfield<tag_trk_hw_phi>();       
    tree.decfield<tag_trk_hw_d0>();        
    tree.decfield<tag_trk_hw_z0>();        
    tree.decfield<tag_trk_hw_beta>();      
    tree.decfield<tag_trk_hw_charge>();    
    
    tree.decfield<tag_trk_model_pattern>();   
    tree.decfield<tag_trk_model_qual>();   
    tree.decfield<tag_trk_model_invpt>();  
    tree.decfield<tag_trk_model_phi>();    
    tree.decfield<tag_trk_model_eta>();    
    tree.decfield<tag_trk_model_d0>();     
    tree.decfield<tag_trk_model_z0>();     
    tree.decfield<tag_trk_model_beta>();   
    tree.decfield<tag_trk_model_features>();   

    tree.decfield<tag_trk_emtf_pt>();      
    tree.decfield<tag_trk_emtf_mode_v1>(); 
    tree.decfield<tag_trk_emtf_mode_v2>(); 
    tree.decfield<tag_vsize_trk>();        

    tree.decfield<tag_trk_site_hits>();
    tree.decfield<tag_trk_site_segs>();
    tree.decfield<tag_trk_site_mask>();
}

void EMTFP2TracksCollector::collect(
        const edm::Event& event, 
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get track collection
    auto handle = event.getHandle(token_);

    const auto track_collection = *reinterpret_cast<const emtf::phase2::EMTFTrackCollection*>(
            handle.product()
    );

    // Collect tracks
    for (const auto& track : track_collection) {
        auto get_hitmode_fn = [](const l1t::phase2::EMTFTrack& track) -> int {
            int n = 0;
            int i = 0;

            for (auto v : track.siteMask()) {
                n |= (v ? (1 << i) : 0);
                i++;
            }

            return n;
        };

        auto get_nhits_fn = [](const l1t::phase2::EMTFTrack& track) -> int {
            int n = std::accumulate(track.siteMask().begin(), track.siteMask().end(), 0);

            return n;
        };

        tree.get<tag_trk_endcap>().push_back(track.endcap());
        tree.get<tag_trk_sector>().push_back(track.sector());
        tree.get<tag_trk_bx>().push_back(track.bx());
        tree.get<tag_trk_invpt>().push_back(track.invpt());
        tree.get<tag_trk_phi>().push_back(0.);                                // not yet implemented
        tree.get<tag_trk_theta>().push_back(0.);                              // not yet implemented
        tree.get<tag_trk_eta>().push_back(0.);                                // not yet implemented
        tree.get<tag_trk_d0>().push_back(0.);                                 // not yet implemented
        tree.get<tag_trk_q>().push_back(0);                                   // not yet implemented
        tree.get<tag_trk_unconstrained>().push_back(track.unconstrained());
        tree.get<tag_trk_hitmode>().push_back(get_hitmode_fn(track));
        tree.get<tag_trk_nhits>().push_back(get_nhits_fn(track));
        tree.get<tag_trk_valid>().push_back(track.valid());

        tree.get<tag_trk_hw_qual>().push_back(track.hwQual());
        tree.get<tag_trk_hw_pt>().push_back(track.hwPt());
        tree.get<tag_trk_hw_eta>().push_back(track.hwEta());
        tree.get<tag_trk_hw_phi>().push_back(track.hwPhi());
        tree.get<tag_trk_hw_d0>().push_back(track.hwD0());
        tree.get<tag_trk_hw_z0>().push_back(track.hwZ0());
        tree.get<tag_trk_hw_beta>().push_back(track.hwBeta());
        tree.get<tag_trk_hw_charge>().push_back(track.hwCharge());

        tree.get<tag_trk_model_pattern>().push_back(track.modelPattern());
        tree.get<tag_trk_model_qual>().push_back(track.modelQual());
        tree.get<tag_trk_model_invpt>().push_back(track.modelInvpt());
        tree.get<tag_trk_model_phi>().push_back(track.modelPhi());
        tree.get<tag_trk_model_eta>().push_back(track.modelEta());
        tree.get<tag_trk_model_d0>().push_back(track.modelD0());
        tree.get<tag_trk_model_z0>().push_back(track.modelZ0());
        tree.get<tag_trk_model_beta>().push_back(track.modelBeta());
        tree.get<tag_trk_model_features>().push_back(track.modelFeatures());

        tree.get<tag_trk_emtf_pt>().push_back(track.emtfPt());
        tree.get<tag_trk_emtf_mode_v1>().push_back(track.emtfModeV1());
        tree.get<tag_trk_emtf_mode_v2>().push_back(track.emtfModeV2());

        tree.get<tag_trk_site_hits>().push_back(track.siteHits());
        tree.get<tag_trk_site_segs>().push_back(track.siteSegs());
        tree.get<tag_trk_site_mask>().push_back(track.siteMask());
    }               

    tree.set<tag_vsize_trk>(track_collection.size());
}

