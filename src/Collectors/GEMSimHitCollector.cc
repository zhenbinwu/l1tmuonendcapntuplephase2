#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/TPUtils.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/GEMSimHitCollector.h"

using namespace emtf::tools;

GEMSimHitCollector::GEMSimHitCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context),
    crossing_frame_en_(pset.getParameter<bool>("CrossingFrameEnabled"))  
{
    if (!crossing_frame_en_) {
        sim_hits_token_ = consumes_collector.consumes<edm::PSimHitContainer>(
                pset.getParameter<edm::InputTag>("GEMSimHitsTag")
        );
    } else {
        sim_hits_xf_token_ = consumes_collector.consumes<CrossingFrame<PSimHit>>(
                pset.getParameter<edm::InputTag>("GEMSimHitsXFTag")
        );
    }

    geom_token_ = consumes_collector.esConsumes<GEMGeometry, MuonGeometryRecord>();
}

GEMSimHitCollector::~GEMSimHitCollector() {
    // Do Nothing
}

void GEMSimHitCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_sim_hit_subsystem>();
    tree.decfield<tag_sim_hit_endcap>();
    tree.decfield<tag_sim_hit_station>();
    tree.decfield<tag_sim_hit_ring>();
    tree.decfield<tag_sim_hit_chamber>();
    tree.decfield<tag_sim_hit_layer>();
    tree.decfield<tag_sim_hit_phi>();
    tree.decfield<tag_sim_hit_theta>();
    tree.decfield<tag_sim_hit_perp>();
    tree.decfield<tag_sim_hit_z>();
    tree.decfield<tag_sim_hit_local_x>();
    tree.decfield<tag_sim_hit_local_y>();
    tree.decfield<tag_sim_hit_trk_part>();
    tree.decfield<tag_sim_hit_pdgid>();
    tree.decfield<tag_sim_hit_process>();
    tree.decfield<tag_sim_hit_mom_p>();
    tree.decfield<tag_sim_hit_mom_phi>();
    tree.decfield<tag_sim_hit_mom_theta>();
    tree.decfield<tag_sim_hit_tof>();
    tree.decfield<tag_vsize_sim_hit>();
}

void GEMSimHitCollector::collect(
        const edm::Event& evt, 
        const edm::EventSetup& evt_setup, 
        DynamicTree& tree
) const {
    // Short-Circuit: Should only collect MC data
    if (evt.isRealData()) {
        return;
    }

    // Declare
    const edm::PSimHitContainer*  sim_hits    = nullptr;
    const CrossingFrame<PSimHit>* sim_hits_xf = nullptr;
    const GEMGeometry*            geom        = nullptr;

    // Get PSimHits
    if (crossing_frame_en_) {
        auto sim_hits_xf_handle = evt.getHandle(sim_hits_xf_token_);
        sim_hits_xf = sim_hits_xf_handle.product();
    } else {
        auto sim_hits_handle = evt.getHandle(sim_hits_token_);
        sim_hits = sim_hits_handle.product();
    }

    // Get Geometry
    auto geom_handle = evt_setup.getHandle(geom_token_);
    geom = geom_handle.product();

    // Collect SimHits
    if (sim_hits_xf != nullptr){
        // Do nothing
    } else if (sim_hits != nullptr) {
        unsigned int n_sim_hits = 0;

        for (const auto& sim_hit : (*sim_hits)) {
            unsigned int det_unit_id    = sim_hit.detUnitId();
            const GEMEtaPartition* roll = geom->etaPartition(det_unit_id);
            const GEMDetId& det_id      = roll->id();
            const LocalPoint& lp        = sim_hit.localPosition();
            const GlobalPoint& gp       = roll->toGlobal(lp);

            // Lookup tp
            auto sim_tp = context_.subsystem_mc_truth_.findTrackingParticleFromSimHit(sim_hit);

            // Append Entry
            tree.get<tag_sim_hit_subsystem>().push_back(L1TMuon::kGEM);
            tree.get<tag_sim_hit_endcap>().push_back(det_id.region());
            tree.get<tag_sim_hit_station>().push_back(det_id.station());
            tree.get<tag_sim_hit_ring>().push_back(det_id.ring());
            tree.get<tag_sim_hit_chamber>().push_back(det_id.chamber());
            tree.get<tag_sim_hit_layer>().push_back(det_id.layer());
            tree.get<tag_sim_hit_phi>().push_back(rad_to_deg(gp.phi().value()));
            tree.get<tag_sim_hit_theta>().push_back(rad_to_deg(gp.theta().value()));
            tree.get<tag_sim_hit_perp>().push_back(gp.perp());
            tree.get<tag_sim_hit_z>().push_back(gp.z());
            tree.get<tag_sim_hit_local_x>().push_back(lp.x());
            tree.get<tag_sim_hit_local_y>().push_back(lp.y());
            tree.get<tag_sim_hit_trk_part>().push_back(sim_tp);
            tree.get<tag_sim_hit_pdgid>().push_back(sim_hit.particleType());
            tree.get<tag_sim_hit_process>().push_back(sim_hit.processType());
            tree.get<tag_sim_hit_mom_p>().push_back(sim_hit.pabs());
            tree.get<tag_sim_hit_mom_phi>().push_back(rad_to_deg(sim_hit.phiAtEntry().value()));
            tree.get<tag_sim_hit_mom_theta>().push_back(rad_to_deg(sim_hit.thetaAtEntry().value()));
            tree.get<tag_sim_hit_tof>().push_back(sim_hit.timeOfFlight());

            ++n_sim_hits;
        }

        tree.get<tag_vsize_sim_hit>() += n_sim_hits;
    }
}

