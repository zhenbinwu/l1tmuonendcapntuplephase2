#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/EventInfoCollector.h"

using namespace emtf::tools;

EventInfoCollector::EventInfoCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    pileup_token_  = consumes_collector.consumes<std::vector<PileupSummaryInfo>>(
            pset.getParameter<edm::InputTag>("PileupInfoTag")  
    );
}

EventInfoCollector::~EventInfoCollector() {
    // Do Nothing
}

void EventInfoCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_evt_event>();
    tree.decfield<tag_evt_run>();
    tree.decfield<tag_evt_lumi>();
    tree.decfield<tag_evt_npv>();
    tree.decfield<tag_evt_nvertices>();
}

void EventInfoCollector::collect(
        const edm::Event& event,
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get Pileup Info
    auto pileup_handle = event.getHandle(pileup_token_);

    const auto pileup_info = reinterpret_cast<const std::vector<PileupSummaryInfo>*>(
            pileup_handle.product()
    );

    // Short-Circuit: If not available leave
    if (pileup_info == nullptr) {
        return;
    }

    // Collect Pileup Info
    float npv = 0;
    int nvertices = 0;

    for (const auto& entry : (*pileup_info)) {
        if (entry.getBunchCrossing() == 0) { // BX=0
            npv = entry.getTrueNumInteractions();
            nvertices = entry.getPU_NumInteractions();
            break;
        }
    }

    // Append Info
    tree.set<tag_evt_event>(event.id().event());
    tree.set<tag_evt_run>(event.id().run());
    tree.set<tag_evt_lumi>(event.id().luminosityBlock());
    tree.set<tag_evt_npv>(npv);
    tree.set<tag_evt_nvertices>(nvertices);
}

