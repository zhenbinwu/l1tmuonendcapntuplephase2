#include <numeric>

#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2InputsCollector.h"

using namespace emtf::tools;

EMTFP2InputsCollector::EMTFP2InputsCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    token_  = consumes_collector.consumes<emtf::phase2::EMTFInputCollection>(
            pset.getParameter<edm::InputTag>("EMTFP2InputTag")  
    );
}

EMTFP2InputsCollector::~EMTFP2InputsCollector() {
    // Do Nothing
}

void EMTFP2InputsCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_in_endcap>();       
    tree.decfield<tag_in_sector>();       
    tree.decfield<tag_in_bx>();           
    tree.decfield<tag_in_hits>();
    tree.decfield<tag_in_segs>();
    tree.decfield<tag_vsize_in>();
}

void EMTFP2InputsCollector::collect(
        const edm::Event& event, 
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get input collection
    auto handle = event.getHandle(token_);

    const auto input_collection = *reinterpret_cast<const emtf::phase2::EMTFInputCollection*>(
            handle.product()
    );

    // Collect inputs
    for (const auto& input : input_collection) {
        tree.get<tag_in_endcap>().push_back(input.endcap());
        tree.get<tag_in_sector>().push_back(input.sector());
        tree.get<tag_in_bx>().push_back(input.bx());
        tree.get<tag_in_hits>().push_back(input.hits());
        tree.get<tag_in_segs>().push_back(input.segs());
    }               

    tree.set<tag_vsize_in>(input_collection.size());
}

