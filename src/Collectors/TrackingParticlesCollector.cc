#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "EMTFTools/NtupleMaker/interface/Utils/EMTFUtils.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/TrackingParticlesCollector.h"

using namespace emtf::tools;

TrackingParticlesCollector::TrackingParticlesCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    token_  = consumes_collector.consumes<TrackingParticleCollection>(
            pset.getParameter<edm::InputTag>("TrkPartTag")  
    );
}

TrackingParticlesCollector::~TrackingParticlesCollector() {
    // Do Nothing
}

void TrackingParticlesCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_trk_part_pt>();
    tree.decfield<tag_trk_part_phi>();
    tree.decfield<tag_trk_part_theta>();
    tree.decfield<tag_trk_part_eta>();
    tree.decfield<tag_trk_part_vx>();
    tree.decfield<tag_trk_part_vy>();
    tree.decfield<tag_trk_part_vz>();
    tree.decfield<tag_trk_part_invpt>();
    tree.decfield<tag_trk_part_d0>();
    tree.decfield<tag_trk_part_beta>();
    tree.decfield<tag_trk_part_mass>();
    tree.decfield<tag_trk_part_q>();
    tree.decfield<tag_trk_part_bx>();
    tree.decfield<tag_trk_part_event>();
    tree.decfield<tag_trk_part_pdgid>();
    tree.decfield<tag_trk_part_status>();
    tree.decfield<tag_trk_part_decay>();
    tree.decfield<tag_trk_part_genp>();
    tree.decfield<tag_trk_part_important>();
    tree.decfield<tag_vsize_trk_part>();
}

void TrackingParticlesCollector::collect(
        const edm::Event& event,
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get Tracking Particles
    auto handle = event.getHandle(token_);

    const auto trk_part_collection = *reinterpret_cast<const TrackingParticleCollection*>(
            handle.product()
    );

    // Collect Traking Particles
    for (const auto& trk_particle : trk_part_collection) {
        int genp = -1;

        if (!trk_particle.genParticles().empty()) {
            auto ref_key = (trk_particle.genParticles().begin())->key();
            genp = ref_key;
        }

        tree.get<tag_trk_part_pt>().push_back(trk_particle.pt());
        tree.get<tag_trk_part_phi>().push_back(trk_particle.phi());
        tree.get<tag_trk_part_theta>().push_back(trk_particle.theta());
        tree.get<tag_trk_part_eta>().push_back(trk_particle.eta());
        tree.get<tag_trk_part_vx>().push_back(trk_particle.vx());
        tree.get<tag_trk_part_vy>().push_back(trk_particle.vy());
        tree.get<tag_trk_part_vz>().push_back(trk_particle.vz());
        tree.get<tag_trk_part_invpt>().push_back(calcInvpt(trk_particle.charge(), trk_particle.pt()));
        tree.get<tag_trk_part_d0>().push_back(calcD0(calcInvpt(trk_particle.charge(), trk_particle.pt()), trk_particle.phi(), trk_particle.vx(), trk_particle.vy()));
        tree.get<tag_trk_part_beta>().push_back(trk_particle.p() / trk_particle.energy());
        tree.get<tag_trk_part_mass>().push_back(trk_particle.mass());
        tree.get<tag_trk_part_q>().push_back(trk_particle.charge());
        tree.get<tag_trk_part_bx>().push_back(trk_particle.eventId().bunchCrossing());
        tree.get<tag_trk_part_event>().push_back(trk_particle.eventId().event());
        tree.get<tag_trk_part_pdgid>().push_back(trk_particle.pdgId());
        tree.get<tag_trk_part_status>().push_back(trk_particle.status());
        tree.get<tag_trk_part_decay>().push_back(trk_particle.decayVertices().size());
        tree.get<tag_trk_part_genp>().push_back(genp);
        tree.get<tag_trk_part_important>().push_back(0); // Set later
    }

    tree.set<tag_vsize_trk_part>(trk_part_collection.size());
}

