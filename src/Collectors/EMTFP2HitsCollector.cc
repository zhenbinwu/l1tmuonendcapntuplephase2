#include "EMTFTools/NtupleMaker/interface/Fields.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

#include "EMTFTools/NtupleMaker/interface/Collectors/EMTFP2HitsCollector.h"

using namespace emtf::tools;

EMTFP2HitsCollector::EMTFP2HitsCollector(
        const NtupleMakerContext& context, 
        const edm::ParameterSet&  pset,
        edm::ConsumesCollector&&  consumes_collector
): 
    context_(context)
{
    token_  = consumes_collector.consumes<emtf::phase2::EMTFHitCollection>(
            pset.getParameter<edm::InputTag>("EMTFP2HitTag")
    );
}

EMTFP2HitsCollector::~EMTFP2HitsCollector() {
    // Do Nothing
}

void EMTFP2HitsCollector::registerFields(DynamicTree& tree) const {
    tree.decfield<tag_hit_id>();             
    tree.decfield<tag_hit_subsystem>();      
    tree.decfield<tag_hit_endcap>();         
    tree.decfield<tag_hit_sector>();         
    tree.decfield<tag_hit_subsector>();      
    tree.decfield<tag_hit_station>();        
    tree.decfield<tag_hit_ring>();           
    tree.decfield<tag_hit_chamber>();        
    tree.decfield<tag_hit_cscid>();          
    tree.decfield<tag_hit_strip>();          
    tree.decfield<tag_hit_strip_lo>();       
    tree.decfield<tag_hit_strip_hi>();       
    tree.decfield<tag_hit_wire1>();          
    tree.decfield<tag_hit_wire2>();          
    tree.decfield<tag_hit_bend>();           
    tree.decfield<tag_hit_quality>();        
    tree.decfield<tag_hit_pattern>();        
    tree.decfield<tag_hit_neighbor>();       
    tree.decfield<tag_hit_cscfr>();          
    tree.decfield<tag_hit_layer>();          
    tree.decfield<tag_hit_subbx>();          
    tree.decfield<tag_hit_bx>();             
    tree.decfield<tag_hit_valid>();          
    tree.decfield<tag_hit_emtf_chamber>();   
    tree.decfield<tag_hit_emtf_segment>();   
    tree.decfield<tag_hit_emtf_phi>();       
    tree.decfield<tag_hit_emtf_bend>();      
    tree.decfield<tag_hit_emtf_theta1>();    
    tree.decfield<tag_hit_emtf_theta2>();    
    tree.decfield<tag_hit_emtf_qual1>();     
    tree.decfield<tag_hit_emtf_qual2>();     
    tree.decfield<tag_hit_emtf_time>();      
    tree.decfield<tag_hit_emtf_site>();      
    tree.decfield<tag_hit_emtf_host>();      
    tree.decfield<tag_hit_emtf_zones>();     
    tree.decfield<tag_hit_emtf_timezones>(); 
    tree.decfield<tag_hit_glob_phi>();       
    tree.decfield<tag_hit_glob_theta>();     
    tree.decfield<tag_hit_glob_perp>();      
    tree.decfield<tag_hit_glob_z>();         
    tree.decfield<tag_hit_glob_time>();      
    tree.decfield<tag_hit_sim_tp1>();        
    tree.decfield<tag_hit_sim_tp2>();        
    tree.decfield<tag_vsize_hit>();          
}

void EMTFP2HitsCollector::collect(
        const edm::Event& event, 
        const edm::EventSetup& event_setup, 
        DynamicTree& tree
) const {
    // Get hit collection
    auto handle = event.getHandle(token_);

    const auto hit_collection = *reinterpret_cast<const emtf::phase2::EMTFHitCollection*>(
            handle.product()
    );

    // Collect hits
    for (const auto& hit : hit_collection) {
        // Lookup tp
        const auto [sim_tp1, sim_tp2] = context_.subsystem_mc_truth_.findTrackingParticlesFromEMTFHit(hit);

        // Append Entry
        tree.get<tag_hit_id>().push_back(hit.id());
        tree.get<tag_hit_subsystem>().push_back(hit.subsystem());
        tree.get<tag_hit_endcap>().push_back(hit.endcap());
        tree.get<tag_hit_sector>().push_back(hit.sector());
        tree.get<tag_hit_subsector>().push_back(hit.subsector());
        tree.get<tag_hit_station>().push_back(hit.station());
        tree.get<tag_hit_ring>().push_back(hit.ring());
        tree.get<tag_hit_chamber>().push_back(hit.chamber());
        tree.get<tag_hit_cscid>().push_back(hit.cscId());
        tree.get<tag_hit_strip>().push_back(hit.strip());
        tree.get<tag_hit_strip_lo>().push_back(hit.stripLo());
        tree.get<tag_hit_strip_hi>().push_back(hit.stripHi());
        tree.get<tag_hit_wire1>().push_back(hit.wire1());
        tree.get<tag_hit_wire2>().push_back(hit.wire2());
        tree.get<tag_hit_bend>().push_back(hit.bend());
        tree.get<tag_hit_quality>().push_back(hit.quality());
        tree.get<tag_hit_pattern>().push_back(hit.pattern());
        tree.get<tag_hit_neighbor>().push_back(hit.flagNeighbor());
        tree.get<tag_hit_cscfr>().push_back(hit.cscFR());
        tree.get<tag_hit_layer>().push_back(hit.layer());
        tree.get<tag_hit_subbx>().push_back(hit.subbx());
        tree.get<tag_hit_bx>().push_back(hit.bx());
        tree.get<tag_hit_valid>().push_back(hit.flagValid());
        tree.get<tag_hit_emtf_chamber>().push_back(hit.emtfChamber());
        tree.get<tag_hit_emtf_segment>().push_back(hit.emtfSegment());
        tree.get<tag_hit_emtf_phi>().push_back(hit.emtfPhi());
        tree.get<tag_hit_emtf_bend>().push_back(hit.emtfBend());
        tree.get<tag_hit_emtf_theta1>().push_back(hit.emtfTheta1());
        tree.get<tag_hit_emtf_theta2>().push_back(hit.emtfTheta2());
        tree.get<tag_hit_emtf_qual1>().push_back(hit.emtfQual1());
        tree.get<tag_hit_emtf_qual2>().push_back(hit.emtfQual2());
        tree.get<tag_hit_emtf_time>().push_back(hit.emtfTime());
        tree.get<tag_hit_emtf_site>().push_back(hit.emtfSite());
        tree.get<tag_hit_emtf_host>().push_back(hit.emtfHost());
        tree.get<tag_hit_emtf_zones>().push_back(hit.emtfZones());
        tree.get<tag_hit_emtf_timezones>().push_back(hit.emtfTimezones());
        tree.get<tag_hit_glob_phi>().push_back(hit.globPhi());
        tree.get<tag_hit_glob_theta>().push_back(hit.globTheta());
        tree.get<tag_hit_glob_perp>().push_back(hit.globPerp());
        tree.get<tag_hit_glob_z>().push_back(hit.globZ());
        tree.get<tag_hit_glob_time>().push_back(hit.globTime());
        tree.get<tag_hit_sim_tp1>().push_back(sim_tp1);
        tree.get<tag_hit_sim_tp2>().push_back(sim_tp2);
    }

    tree.set<tag_vsize_hit>(hit_collection.size());
}

