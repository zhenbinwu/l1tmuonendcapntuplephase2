#ifndef EMTFTools_NtupleMaker_NtupleMakerContext_h
#define EMTFTools_NtupleMaker_NtupleMakerContext_h

#include "EMTFTools/NtupleMaker/interface/Simulations/GenParticleDescendentFinder.h"
#include "EMTFTools/NtupleMaker/interface/Simulations/SubsystemMCTruth.h"

namespace emtf::tools {

    class NtupleMakerContext {

        public:
            NtupleMakerContext(
                    const edm::ParameterSet&,
                    edm::ConsumesCollector
            );

            ~NtupleMakerContext();

            // Event configuration
            void update(
                    const edm::Event&,
                    const edm::EventSetup&
            );

            // Helpers
            GenParticleDescendentFinder gen_part_descendent_finder_;
            SubsystemMCTruth            subsystem_mc_truth_;
    };

}

#endif  // EMTFTools_NtupleMaker_NtupleMakerContext_h not defined
