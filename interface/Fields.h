#ifndef EMTFTools_NtupleMaker_Fields_h
#define EMTFTools_NtupleMaker_Fields_h

#include <string>
#include <vector>

#define DEFINE_FIELD(TYPE, NAME) \
    struct tag_##NAME { \
        typedef TYPE type; \
        static constexpr const char* name = #NAME; \
    };

namespace emtf::tools {
    // Event Info
    DEFINE_FIELD(uint64_t, evt_event);                                  //
    DEFINE_FIELD(uint32_t, evt_run);                                    //
    DEFINE_FIELD(uint32_t, evt_lumi);                                   //
    DEFINE_FIELD(float,    evt_npv);                                    // getTrueNumInteractions()
    DEFINE_FIELD(int16_t,  evt_nvertices);                              // getPU_NumInteractions()
                                                                        //
    // EMTF Inputs
    DEFINE_FIELD(std::vector<uint16_t>,               in_endcap);       //
    DEFINE_FIELD(std::vector<uint16_t>,               in_sector);       //
    DEFINE_FIELD(std::vector<uint16_t>,               in_bx);           //
    DEFINE_FIELD(std::vector<std::vector<uint16_t>>,  in_hits);         //
    DEFINE_FIELD(std::vector<std::vector<uint16_t>>,  in_segs);         //
    DEFINE_FIELD(int16_t,                             vsize_in);        //

    // EMTF Hits
    DEFINE_FIELD(std::vector<uint16_t>, hit_id);                        //
    DEFINE_FIELD(std::vector<int16_t>,  hit_subsystem);                 // kDT, kCSC, kRPC, kGEM, kME0
    DEFINE_FIELD(std::vector<int16_t>,  hit_endcap);                    //
    DEFINE_FIELD(std::vector<int16_t>,  hit_sector);                    //
    DEFINE_FIELD(std::vector<int16_t>,  hit_subsector);                 //
    DEFINE_FIELD(std::vector<int16_t>,  hit_station);                   //
    DEFINE_FIELD(std::vector<int16_t>,  hit_ring);                      //
    DEFINE_FIELD(std::vector<int16_t>,  hit_chamber);                   //
    DEFINE_FIELD(std::vector<int16_t>,  hit_cscid);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_strip);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_strip_lo);                  //
    DEFINE_FIELD(std::vector<int16_t>,  hit_strip_hi);                  //
    DEFINE_FIELD(std::vector<int16_t>,  hit_wire1);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_wire2);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_bend);                      //
    DEFINE_FIELD(std::vector<int16_t>,  hit_quality);                   //
    DEFINE_FIELD(std::vector<int16_t>,  hit_pattern);                   //
    DEFINE_FIELD(std::vector<int16_t>,  hit_neighbor);                  //
    DEFINE_FIELD(std::vector<int16_t>,  hit_cscfr);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_layer);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_subbx);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_bx);                        //
    DEFINE_FIELD(std::vector<int16_t>,  hit_valid);                     //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_chamber);              //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_segment);              //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_phi);                  //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_bend);                 //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_theta1);               //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_theta2);               //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_qual1);                //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_qual2);                //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_time);                 //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_site);                 //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_host);                 //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_zones);                //
    DEFINE_FIELD(std::vector<int16_t>,  hit_emtf_timezones);            //
    DEFINE_FIELD(std::vector<float>,    hit_glob_phi);                  // in degrees
    DEFINE_FIELD(std::vector<float>,    hit_glob_theta);                // in degrees
    DEFINE_FIELD(std::vector<float>,    hit_glob_perp);                 // in cm
    DEFINE_FIELD(std::vector<float>,    hit_glob_z);                    // in cm
    DEFINE_FIELD(std::vector<float>,    hit_glob_time);                 // in ns
    DEFINE_FIELD(std::vector<int32_t>,  hit_sim_tp1);                   //
    DEFINE_FIELD(std::vector<int32_t>,  hit_sim_tp2);                   //
    DEFINE_FIELD(int32_t,               vsize_hit);                     //

    // Sim Hits
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_subsystem);              // kDT, kCSC, kRPC, kGEM, kME0
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_endcap);                 // 
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_station);                //
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_ring);                   //
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_chamber);                //
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_layer);                  //
    DEFINE_FIELD(std::vector<float>,   sim_hit_phi);                    // in degrees
    DEFINE_FIELD(std::vector<float>,   sim_hit_theta);                  // in degrees
    DEFINE_FIELD(std::vector<float>,   sim_hit_perp);                   // in cm
    DEFINE_FIELD(std::vector<float>,   sim_hit_z);                      // in cm
    DEFINE_FIELD(std::vector<float>,   sim_hit_local_x);                //
    DEFINE_FIELD(std::vector<float>,   sim_hit_local_y);                //
    DEFINE_FIELD(std::vector<int32_t>, sim_hit_trk_part);               //
    DEFINE_FIELD(std::vector<int32_t>, sim_hit_pdgid);                  // particleType()
    DEFINE_FIELD(std::vector<int16_t>, sim_hit_process);                // processType()
    DEFINE_FIELD(std::vector<float>,   sim_hit_mom_p);                  // pabs()
    DEFINE_FIELD(std::vector<float>,   sim_hit_mom_phi);                // phiAtEntry()
    DEFINE_FIELD(std::vector<float>,   sim_hit_mom_theta);              // thetaAtEntry()
    DEFINE_FIELD(std::vector<float>,   sim_hit_tof);                    // timeOfFlight()
    DEFINE_FIELD(int32_t,              vsize_sim_hit);                  //

    // EMTF Tracks
    DEFINE_FIELD(std::vector<int16_t>, trk_endcap);                     //
    DEFINE_FIELD(std::vector<int16_t>, trk_sector);                     //
    DEFINE_FIELD(std::vector<int16_t>, trk_bx);                         //
    DEFINE_FIELD(std::vector<float>,   trk_invpt);                      //
    DEFINE_FIELD(std::vector<float>,   trk_phi);                        // in radians
    DEFINE_FIELD(std::vector<float>,   trk_theta);                      // in radians
    DEFINE_FIELD(std::vector<float>,   trk_eta);                        //
    DEFINE_FIELD(std::vector<float>,   trk_d0);                         // in cm
    DEFINE_FIELD(std::vector<int16_t>, trk_q);                          // charge
    DEFINE_FIELD(std::vector<int16_t>, trk_unconstrained);              //
    DEFINE_FIELD(std::vector<int16_t>, trk_hitmode);                    //
    DEFINE_FIELD(std::vector<int16_t>, trk_nhits);                      //
    DEFINE_FIELD(std::vector<int16_t>, trk_valid);                      //
    DEFINE_FIELD(std::vector<int16_t>, trk_hw_qual);                    //
    DEFINE_FIELD(std::vector<int32_t>, trk_hw_pt);                      //
    DEFINE_FIELD(std::vector<int32_t>, trk_hw_eta);                     //
    DEFINE_FIELD(std::vector<int32_t>, trk_hw_phi);                     //
    DEFINE_FIELD(std::vector<int16_t>, trk_hw_d0);                      //
    DEFINE_FIELD(std::vector<int16_t>, trk_hw_z0);                      //
    DEFINE_FIELD(std::vector<int16_t>, trk_hw_beta);                    //
    DEFINE_FIELD(std::vector<int16_t>, trk_hw_charge);                  //
    DEFINE_FIELD(std::vector<int16_t>, trk_model_pattern);              //
    DEFINE_FIELD(std::vector<int16_t>, trk_model_qual);                 //
    DEFINE_FIELD(std::vector<int32_t>, trk_model_invpt);                //
    DEFINE_FIELD(std::vector<int32_t>, trk_model_phi);                  //
    DEFINE_FIELD(std::vector<int32_t>, trk_model_eta);                  //
    DEFINE_FIELD(std::vector<int16_t>, trk_model_d0);                   //
    DEFINE_FIELD(std::vector<int16_t>, trk_model_z0);                   //
    DEFINE_FIELD(std::vector<int16_t>, trk_model_beta);                 //
    DEFINE_FIELD(std::vector<int32_t>, trk_emtf_pt);                    //
    DEFINE_FIELD(std::vector<int16_t>, trk_emtf_mode_v1);               //
    DEFINE_FIELD(std::vector<int16_t>, trk_emtf_mode_v2);               //
    DEFINE_FIELD(int32_t,              vsize_trk);                      //

    DEFINE_FIELD(std::vector<std::vector<int16_t>>,  trk_model_features);
    DEFINE_FIELD(std::vector<std::vector<uint16_t>>, trk_site_hits);
    DEFINE_FIELD(std::vector<std::vector<uint16_t>>, trk_site_segs);
    DEFINE_FIELD(std::vector<std::vector<uint8_t>>,  trk_site_mask);

    // L1TrackTrigger Tracks
    DEFINE_FIELD(std::vector<float>,   l1t_trk_pt);                      //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_phi);                     // in radians
    DEFINE_FIELD(std::vector<float>,   l1t_trk_theta);                   // in radians
    DEFINE_FIELD(std::vector<float>,   l1t_trk_eta);                     //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_vx);                      // POCA, in cm
    DEFINE_FIELD(std::vector<float>,   l1t_trk_vy);                      // POCA, in cm
    DEFINE_FIELD(std::vector<float>,   l1t_trk_vz);                      // POCA, in cm
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_q);                       // charge
    DEFINE_FIELD(std::vector<float>,   l1t_trk_rinv);                    //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_chi2);                    // in cm
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_ndof);                    //
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_phi_sector);              //
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_eta_sector);              //
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_hit_pattern);             //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_sim_pt);                  //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_sim_phi);                 //
    DEFINE_FIELD(std::vector<float>,   l1t_trk_sim_eta);                 //
    DEFINE_FIELD(std::vector<int32_t>, l1t_trk_sim_tp);                  //
    DEFINE_FIELD(std::vector<int32_t>, l1t_trk_pdgid);                   //
    DEFINE_FIELD(std::vector<int16_t>, l1t_trk_genuine);                 // isUnknown, isCombinatoric, isLooselyGenuine, isGenuine
    DEFINE_FIELD(int32_t,              vsize_l1t_trk);                   //

    // Tracking Particles
    DEFINE_FIELD(std::vector<float>,   trk_part_pt);                    //
    DEFINE_FIELD(std::vector<float>,   trk_part_phi);                   // in radians
    DEFINE_FIELD(std::vector<float>,   trk_part_theta);                 // in radians
    DEFINE_FIELD(std::vector<float>,   trk_part_eta);                   //
    DEFINE_FIELD(std::vector<float>,   trk_part_vx);                    // in cm
    DEFINE_FIELD(std::vector<float>,   trk_part_vy);                    // in cm
    DEFINE_FIELD(std::vector<float>,   trk_part_vz);                    // in cm
    DEFINE_FIELD(std::vector<float>,   trk_part_invpt);                 //
    DEFINE_FIELD(std::vector<float>,   trk_part_d0);                    // in cm
    DEFINE_FIELD(std::vector<float>,   trk_part_beta);                  //
    DEFINE_FIELD(std::vector<float>,   trk_part_mass);                  //
    DEFINE_FIELD(std::vector<int16_t>, trk_part_q);                     // charge
    DEFINE_FIELD(std::vector<int16_t>, trk_part_bx);                    //
    DEFINE_FIELD(std::vector<int16_t>, trk_part_event);                 //
    DEFINE_FIELD(std::vector<int32_t>, trk_part_pdgid);                 //
    DEFINE_FIELD(std::vector<int16_t>, trk_part_status);                //
    DEFINE_FIELD(std::vector<int16_t>, trk_part_decay);                 //
    DEFINE_FIELD(std::vector<int32_t>, trk_part_genp);                  //
    DEFINE_FIELD(std::vector<int16_t>, trk_part_important);             // descend from W/Z/H
    DEFINE_FIELD(int32_t,              vsize_trk_part);                 //
}

#undef DEFINE_FIELD

#endif // namespace EMTFTools_NtupleMaker_Fields_h

