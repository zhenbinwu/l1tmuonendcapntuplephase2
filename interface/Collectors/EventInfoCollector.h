#ifndef EMTFTools_NtupleMaker_EventInfoCollector_h
#define EMTFTools_NtupleMaker_EventInfoCollector_h

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"

namespace emtf::tools {

    class EventInfoCollector: public DataCollector {
        public:
            EventInfoCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&,
                    edm::ConsumesCollector&&
            );

            ~EventInfoCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            edm::EDGetTokenT<std::vector<PileupSummaryInfo>> pileup_token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_EventInfoCollector_h
