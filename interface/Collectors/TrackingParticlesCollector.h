#ifndef EMTFTools_NtupleMaker_TrackingParticlesCollector_h
#define EMTFTools_NtupleMaker_TrackingParticlesCollector_h

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "EMTFTools/NtupleMaker/interface/NtupleMakerContext.h"
#include "EMTFTools/NtupleMaker/interface/Collectors/DataCollectors.h"
#include "SimDataFormats/TrackingAnalysis/interface/TrackingParticle.h"
#include "SimDataFormats/TrackingAnalysis/interface/TrackingParticleFwd.h"

namespace emtf::tools {

    class TrackingParticlesCollector: public DataCollector {
        public:
            TrackingParticlesCollector(
                    const NtupleMakerContext&,
                    const edm::ParameterSet&,
                    edm::ConsumesCollector&&
            );

            ~TrackingParticlesCollector();

            void registerFields(DynamicTree&) const final;

            void collect(
                    const edm::Event&, const edm::EventSetup&, 
                    DynamicTree&
            ) const final;

        private:
            const NtupleMakerContext& context_;

            edm::EDGetTokenT<TrackingParticleCollection> token_;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_TrackingParticlesCollector_h
