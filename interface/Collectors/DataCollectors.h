#ifndef EMTFTools_NtupleMaker_DataCollectors_h 
#define EMTFTools_NtupleMaker_DataCollectors_h

#include "EMTFTools/NtupleMaker/interface/DynamicTree.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFfwd.h"
#include "L1Trigger/L1TMuonEndCapPhase2/interface/EMTFTypes.h"

namespace emtf::tools {

    class DataCollector {
        public:
            DataCollector() = default;

            virtual ~DataCollector() = default;

            virtual void registerFields(DynamicTree&) const = 0;

            virtual void collect(
                    const edm::Event&, 
                    const edm::EventSetup&, 
                    DynamicTree&
            ) const = 0;
    };

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_DataCollectors_h
