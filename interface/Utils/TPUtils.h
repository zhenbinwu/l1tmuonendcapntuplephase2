#ifndef EMTFTools_NtupleMaker_TPUtils_h
#define EMTFTools_NtupleMaker_TPUtils_h

namespace emtf::tools {

    float deg_to_rad(float deg);

    float rad_to_deg(float rad);

}

#endif // namespace EMTFTools_NtupleMaker_TPUtils_h
