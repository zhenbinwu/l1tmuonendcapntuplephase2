#ifndef EMTFTools_NtupleMaker_EMTFUtils_h
#define EMTFTools_NtupleMaker_EMTFUtils_h

namespace emtf::tools {

    double calcInvpt(const double&, const double&);

    double calcD0(const double&, const double&, const double&, const double&);

}  // namespace emtf::tools

#endif  // EMTFTools_NtupleMaker_EMTFUtils_h not defined
