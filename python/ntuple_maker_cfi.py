
import FWCore.ParameterSet.Config as cms

# EMTF Tools Ntuple Maker Configuration
emtfToolsNtupleMaker = cms.EDAnalyzer(
    "EMTFToolsNtupleMaker",

    # Verbosity level
    verbosity = cms.untracked.int32(3),

    # Enables
    EventInfoEnabled = cms.bool(True),
    EMTFP2HitsEnabled = cms.bool(True),
    EMTFP2TracksEnabled = cms.bool(True),
    EMTFP2InputsEnabled = cms.bool(True),
    L1TrackTriggerTracksEnabled = cms.bool(True),
    TrackingParticlesEnabled = cms.bool(True),
    CSCSimHitEnabled = cms.bool(True),
    RPCSimHitEnabled = cms.bool(True),
    GEMSimHitEnabled = cms.bool(True),
    ME0SimHitEnabled = cms.bool(True),

    # Alt Enables
    CrossingFrameEnabled = cms.bool(False),

    # Tags
    GenPartTag = cms.InputTag('genParticles'),
    simTrackTag = cms.InputTag('g4SimHits'),
    TrkPartTag = cms.InputTag('mix', 'MergedTrackTruth'),
    PileupInfoTag = cms.InputTag('addPileupInfo'),
    EMTFP2HitTag = cms.InputTag('simEmtfDigisPhase2'),
    EMTFP2TrackTag = cms.InputTag('simEmtfDigisPhase2'),
    EMTFP2InputTag = cms.InputTag('simEmtfDigisPhase2'),
    TTTFTrackTag = cms.InputTag('TTTracksFromTrackletEmulation', 'Level1TTTracks'),
    TTTFTrackAssocTag = cms.InputTag('TTTrackAssociatorFromPixelDigis', 'Level1TTTracks'),

    CSCSimHitsTag = cms.InputTag('g4SimHits', 'MuonCSCHits'),
    CSCSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonCSCHits'),
    RPCSimHitsTag = cms.InputTag('g4SimHits', 'MuonRPCHits'),
    RPCSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonRPCHits'),
    GEMSimHitsTag = cms.InputTag('g4SimHits', 'MuonGEMHits'),
    GEMSimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonGEMHits'),
    ME0SimHitsTag = cms.InputTag('g4SimHits', 'MuonME0Hits'),
    ME0SimHitsXFTag = cms.InputTag('mix', 'g4SimHitsMuonME0Hits'),

    CSCStripSimLinksTag = cms.InputTag('simMuonCSCDigis', 'MuonCSCStripDigiSimLinks'),
    CSCWireSimLinksTag  = cms.InputTag('simMuonCSCDigis', 'MuonCSCWireDigiSimLinks'),
    RPCDigiSimLinksTag  = cms.InputTag('simMuonRPCDigis', 'RPCDigiSimLink'),
    GEMDigiSimLinksTag  = cms.InputTag('simMuonGEMDigis', 'GEM'),
    ME0DigiSimLinksTag  = cms.InputTag('simMuonME0Digis', 'ME0'),
)

